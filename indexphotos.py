#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import PIL
import colorama
from PIL import Image
from colorama import Fore

# Réinitialisation de la couleur après chaque 'print'
colorama.init(autoreset=True)

# Test pour vérifier si la saisie d'un nom de dossier est correcte
if len(sys.argv) < 2:
    print(Fore.RED + '------------------------------------------------')
    print(Fore.RED + 'Veuillez préciser le nom du dossier à traiter...')
    print(Fore.RED + '------------------------------------------------')
    exit(0)

# Récupération du nom du dossier
folder = os.path.abspath(sys.argv[1])

# Test pour vérifier si le dossier est correct
if os.path.exists(folder) != True or os.path.isfile(folder) == True:
    print(Fore.RED + '--------------------------------------')
    print(Fore.RED + 'Veuillez indiquer un dossier valide...')
    print(Fore.RED + '--------------------------------------')
    exit(0)

# Création de la liste des dates
list_date = []
for filename in os.listdir(folder):
    if filename.endswith('.jpg') or filename.endswith('.JPG'):
        photo = PIL.Image.open(folder + '/' + filename)
        exifdata = photo._getexif()[36867]
        date_image = exifdata.split(' ')[0]
        date_clean = date_image.replace(':', '.')
        list_date.append(date_clean)
list_clean = sorted(list(set(list_date)))
if len(list_clean) == 1:
    print(Fore.CYAN + 'Une seule date a été identifiée dans ' + str(len(list_date)) + ' photo(s).')
else:
    print(Fore.CYAN + str(len(list_clean)) + ' dates différentes ont été identifiées dans ' + str(len(list_date)) + ' photos.')

# Création des dossiers d'après la liste des dates
for sub_folder in list_clean:
    os.mkdir(folder + '/' + sub_folder)
if len(list_clean) == 1:
    print(Fore.CYAN + 'Un seul dossier a été créé.')
else:
    print(Fore.CYAN + str(len(list_clean)) + ' dossiers ont été créés.')

# Requête auprès de l'utilisateur (permet une interruption momentanée du script)
while True:
    answer = input('Voulez-vous continuer ? [O/n] ')
    if answer.lower() in ['o', 'oui', '']:
        break
    elif answer.lower() in ['n', 'non']:
        print(Fore.RED + '======================')
        print(Fore.RED + 'Programme interrompu !')
        print(Fore.RED + '======================')
        exit(0)
    else:
        print(Fore.RED + '-------------------')
        print(Fore.RED + 'Choix non valide...')
        print(Fore.RED + '-------------------')

# Copie des fichiers vers les dossiers
for sub_folder in list_clean:
    for filename in sorted(os.listdir(folder)):
        if filename.endswith('.jpg') or filename.endswith('.JPG'):
            photo = PIL.Image.open(folder + '/' + filename)
            exifdata = photo._getexif()[36867]
            date_image = exifdata.split(' ')[0]
            date_clean = date_image.replace(':', '.')
            if date_clean == sub_folder:
                shutil.copy2(folder + '/' + filename, folder + '/' + sub_folder)
                print(Fore.YELLOW + filename + Fore.GREEN + ' a été copié vers ' + Fore.YELLOW + str(sub_folder) + Fore.GREEN + ' avec succès !')

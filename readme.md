# indexphotos.py

Ce script permet de trier les photos contenues dans un répertoire, par date de prise de vue.

## Prérequis

  * python3-pillow
  * python3-colorama

## Utilisation

Le lancement s'effectue dans un terminal :

```
$ python3 indexphotos.py /chemin/vers/mon/dossier/
```

## À noter

* la recherche de photos n'est pas récursive
* si les photos ne contiennent pas de données EXIF, le script s'arrête
* seules les photos ayant les extensions **.jpg** ou **.JPG** sont traitées

## Fonctionnement

Le programme va alors :

1. lister les dates contenues dans les photos
2. créer des dossiers d'après ces dates
3. copier les photos dans ces dossiers

## Attention

Le programme effectue simplement une copie des photos, il ne les déplace pas.  
Il faut donc prévoir un espace de stockage suffisant.

## Licence

Le programme ci-joint est publié sous licence GPLv3.  
[www.gnu.org/licenses/gpl-3.0.fr.html](https://www.gnu.org/licenses/gpl-3.0.fr.html)
